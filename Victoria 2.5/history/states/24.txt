
state={
	id=24
	name="STATE_24"
	resources={
		oil=1.000
		steel=12.000
	}

	history={
		owner = AUS
		victory_points = {
			3569 3 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 0
			arms_factory = 0
			air_base = 0

		}
		add_core_of = AUS
		add_core_of = CZE
		add_core_of = X19
	}

	provinces={
		421 557 569 3414 3553 3569 3583 6485 6562 6576 6590 9551 9567 11511 
	}
	manpower=1696408
	buildings_max_level_factor=1.000
	state_category=rural
}
