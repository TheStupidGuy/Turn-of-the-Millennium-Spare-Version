
state={
	id=614
	name="South Pacific Islands"
	resources={
		chromium=119.000
	}

	history={
	owner = UNC
		add_core_of = UNC
		victory_points = {
			12148 1 
		}
		buildings = {
			infrastructure = 0
			industrial_complex = 0
			arms_factory = 0
			air_base = 0

		}
	}

	provinces={
		1263 4231 4268 4286 7264 7302 10160 10188 12132 12148 12159 
	}
	manpower=56000
	buildings_max_level_factor=1.000
	state_category=pastoral
}
